### MATERIAL COM O OBJETIVO DE INSTALAÇÃO DO JENKINS VIA DOCKER COMPOSE E VIA KUBERNETES ###

##  IMAGEM CUSTOMIZADA DO JENKINS (JAVA8) ##
Clonar repositorio:
    git clone ....
    cd ASDASDASD./dockerfile
    docker build -t nome_repositorio_docker/nome_imagem_desejada:versao .
    docker push nome_repositorio_docker/nome_imagem_desejada:versao

obs: imagem customizada com versao do java mais antiga, pois para esse lab no momento de realizar
     um deploy de um manifesto no jenkins, que foi instalado no k8s(kind) ele encontrava erros de incompatibilidade com o java.
=================================================================================================================================
## INSTALACAO SIMPLES DO JENKINS VIA DOCKER COMPOSE ##
INSTALAÇÃO DO JENKINS VIA DOCKER-COMPOSE, COM A IMAGEM CUSTOMIZADA(JAVA8) em dockerfile/Dockerfile

Clonar repositorio:
    git clone ....
    cd ASDASDASD./compose
    docker-compose-up -d
    acessar browser na porta 8080

OBS: SE ATENTAR EM ALTERAR O VOLUME LOCAL NO ARQUIVO COMPOSE/DOCKER-COMPOSE.YAML
EX:
    DIRETORIO_LOCAL:/var/jenkins_home
=================================================================================================================================
## INSTALACAO DO JENKINS VIA K8S (KIND) ##
CRIAR CLUSTER K8S UTILIZANDO O KIND

Clonar repositorio:
    git clone ....
    cd ASDASDASD./kind
    kind create cluster --config kind.yaml --name k8s 

VERIFICAR INFO DO CLUSTER
kubctl cluster-info

FAZER DEPLOY DO JENKINS NO K8S
cd k8s/
kubectl apply -f pv.yaml -f pvc.yaml
kubectl apply -f deployment.yaml 
kubectl apply -f service.yaml 
kubectl apply -f serviceaccount.yaml

NESSE CASO NOSSO SERVICE ESTÁ CONFIGURADO COMO "NODEPORT" PEGAR INFO DO NODE E PORT PARA ACESSAR VIA NAVEGADOR
kubectl get nodes -o wide
(ip)

kubectl get svc
(port)

ACESSAR PELO NAVEGAR COM AS INFOS DOS 2 COMANDOS ACIMA

COMANDO PARA DELETAR CLUSTER
kind delete clusters k8s

